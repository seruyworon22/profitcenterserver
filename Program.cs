﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml;

namespace ProfitCenterServer
{
    class Program
    {/// <summary>
    ////Сервер отправляющий значения
    /// </summary>
    /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {
                SendMessage(); // отправляем сообщение
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {//чтобы консолька не закрылась при ошибке
                Console.ReadKey();
            }
        }
        /// <summary>
        /// Читает XML файл с настройками
        /// </summary>
        /// <returns></returns>
        static Serverconf ReadXmlFile()
        {
            Serverconf config = new Serverconf();
            XmlDocument xDoc = new XmlDocument();
            //файл лежит в дирректории с ехе
            xDoc.Load("setting.xml");
            //получаем корневой узел
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode xnode in xRoot)
            {
                //обходим узел по вложенным
                foreach (XmlNode childnode in xnode.ChildNodes)
                {
                    //разбираем значения из файла в класс
                    if (childnode.Name == "ip")
                    {
                        config.ip = IPAddress.Parse(childnode.InnerText);
                    }
                    if (childnode.Name == "port")
                    {
                        config.port = Convert.ToInt32( childnode.InnerText);
                    }
                    if (childnode.Name == "minvalue")
                    {
                        config.minvalue = Convert.ToInt32(childnode.InnerText);
                    }
                    if (childnode.Name == "maxvalue")
                    {
                        config.maxvalue = Convert.ToInt32(childnode.InnerText);
                    }
                }
            }
            return config;
        }

        private static void SendMessage()
        {
            UdpClient sender = new UdpClient(); // создаем UdpClient для отправки
            Serverconf config = ReadXmlFile(); //получаем параметры        
            IPEndPoint endPoint = new IPEndPoint(config.ip, config.port);//конечная точка
            Random rnd = new Random();
            try
            {//бесконечно генерим и отсылаем данные
                long packeges = 1;// трилион и выше, вдруг вылезет за пределы инта
                while (true)
                {
                    //Thread.Sleep(1000);//иначе у меня ложится сеть и ничего кроме проги уже не работает
                    //Я по сути никогда с сетью не работал, и не имею представления как правильно контрольровать пакеты в udp
                    //Поэтому использую такой велосипед. Отправлю значение и номер пакета а на клиенте разберу
                    string value = rnd.Next(config.minvalue, config.maxvalue).ToString()+";"+ packeges.ToString();//генерим значение
                    byte[] data = Encoding.Unicode.GetBytes(value);//разбиваем для отправки
                    sender.Send(data, data.Length, endPoint); // отправка
                    packeges++;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {// если чтото пошло не так закрываем клиент
                sender.Close();
            }
        }

    }
    /// <summary>
    /// Настройки сервера
    /// </summary>
    class Serverconf
    {
        //хост для отправки
       public IPAddress ip { get; set; }
        //порт отправки
       public int port { get; set; }
        //нижняя граница диапазона
       public int minvalue { get; set; }
        //верхняя граница диапазона
       public int maxvalue { get; set; }
    }
}
